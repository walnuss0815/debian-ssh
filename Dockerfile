FROM debian:stable-slim

RUN apt-get update \
 && apt-get install openssh-client -y --no-install-recommends \
 && rm -rf /var/lib/apt/lists/* \
 && useradd -ms /bin/bash debian

USER debian

WORKDIR /home/debian